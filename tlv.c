// TLV processing code -- assessment exercise
//
//   6/2/13 Ravi Chandra <ravi@chandra.cc>
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

// basic boolean type
typedef uint8_t BOOL;
#define TRUE 1
#define FALSE 0

// TAG field
#define CONSTR_BIT 1<<5   // 0 = primitive, 1 = constructed
#define SINGLE_TAG_BYTE_MASK 0x1F
#define FOLLOWS_TAG_BYTE_MASK 0x80
#define FOLLOWS_LEN_BYTE_MASK 0x80

typedef struct {
    uint16_t tag;
    size_t val_len;
    size_t buff_len; // incase data buffer is larger than the TLV value
    uint8_t* buff; // not const because we update it
} Tlv_t;

typedef struct {
    char TxnRef[16+1]; // NULL terminated string
    int32_t amount;
    uint8_t txnType;
    uint16_t currencyCode;
} TxnInfo_t;

void PrintBufferHex(const uint8_t* buff, size_t len)
{
    size_t i, j;

    for (i=0, j=0; i<len; i++) {
        printf("%02x ", buff[i]);
        if (++j%8 == 0) printf("\n");
    }
    if (j%8 != 0) printf("\n");
    printf("\n");
}

void Tlv_Debug(Tlv_t* tlv)
{
    int i, j, k;
    char ascii_buf[8+1];

    printf("> tlv tag: %x\n", tlv->tag);
    printf("> tlv len: %d\n", tlv->val_len);
    printf("> tlv data:\n");
    for (i=0, j=0, k=0; i<tlv->val_len; i++) {
        printf("%02x ", (uint8_t)tlv->buff[i]);
        ascii_buf[k++] = ((tlv->buff[i] >= 0x20) && (tlv->buff[i] < 0x7f)) ? tlv->buff[i] : '.';
        if (++j%8 == 0) {
            ascii_buf[k] = '\0';
            printf("\t%s\n", ascii_buf);
            k = 0;
        }
    }
    if (j%8 != 0) {
        ascii_buf[k] = '\0';
        for (i=k; i<8; i++) printf("   ");
        printf("\t%s\n", ascii_buf);
        k = 0;
    }
    printf("\n");
}

// helper function to scan the tag ID + length field and then
// return a pointer to the beginning of value data
const uint8_t* ParseTlvHeader(const uint8_t* buffer, size_t length, Tlv_t* tlv)
{
    size_t i, j;
    uint8_t b;
    uint8_t len_bytes;
    uint8_t state = 0;

    for (i=0; i<length; i++) {
        // read next byte and process it with state machine
        b = buffer[i];

        switch (state) {
        case 0:
            // looking for tag byte #1
            if ((b & SINGLE_TAG_BYTE_MASK) < SINGLE_TAG_BYTE_MASK) {
                tlv->tag = b;
                state = 2;
            } else if ((b & SINGLE_TAG_BYTE_MASK) == SINGLE_TAG_BYTE_MASK) {
                tlv->tag = b;
                state = 1;
            }
            break;
        case 1:
            // looking for tag byte #2
            if (!(b & FOLLOWS_TAG_BYTE_MASK)) {
                tlv->tag = (tlv->tag << 8) | b;
                state = 2;
            } else {
                // unable to parse this properly!
                return FALSE;
            }
            break;
        case 2:
            // looking for length byte 1
            if (!(b & FOLLOWS_LEN_BYTE_MASK)) {
                tlv->val_len = b;
                state = 4;
            } else {
                // set up counters for variable-length length field
                j = 0;
                len_bytes = (b & 0x7F);
                state = 3;
            }
            break;
        case 3:
            // looking for more length bytes
            tlv->val_len = (tlv->val_len << 8) | b;
            if (++j == len_bytes) {
                state = 4;
            }
            break;
        case 4:
            if (length-i < tlv->val_len) {
                // buffer doesn't have enough data left? give up!
                return FALSE;
            } else {
                return &buffer[i];
            }
        }
    }
    return NULL;
}

// helper function to allocate and copy value data from a buffer into a TLV
// extracted mainly for debugging use in the TlvSearchTag function
BOOL CopyTlvValueData(const uint8_t* buffer, Tlv_t* tlv)
{
        // allocate buffer and copy the value data in
        tlv->buff_len = tlv->val_len;
        tlv->buff = (uint8_t*)malloc(sizeof(uint8_t)*tlv->buff_len);
        if (tlv->buff == NULL) return FALSE;
        memcpy(tlv->buff, buffer, tlv->val_len);
        return TRUE;
}

//
// Parse the data in buffer to a TLV object
//   ! copies data from `buffer' into the new TLV object !
BOOL TlvParse(const uint8_t* buffer, size_t length, Tlv_t* tlv)
{
    size_t i, j;
    uint8_t b;
    uint8_t len_bytes;
    uint8_t state = 0;

    const uint8_t* value_buffer;

    value_buffer = ParseTlvHeader(buffer, length, tlv);
    if (value_buffer == NULL) {
        // something went wrong and we couldn't parse the header properly?!
        return FALSE;
    } else {
        // allocate buffer and copy the value data in
        return CopyTlvValueData(value_buffer, tlv);
    }
}

// is this a primitive TLV? (if not, then its constructed!)
BOOL TlvPrimitive(Tlv_t* tlv)
{
    uint8_t z;
    if (tlv->tag > 0xff) z = (tlv->tag >> 8) & 0xff;
    else z = tlv->tag;
    return (z & CONSTR_BIT) ? FALSE : TRUE;
}

// return the number of bytes required to represent tag+len fields
uint8_t GetTlvHeaderSize(const Tlv_t* tlv)
{
    // we know the tag is either 1 or 2 bytes so we can check easily
    uint8_t tag_len = tlv->tag <= 0xff ? 1 : 2;
    // the length field can be longer, add extra byte if > 127
    uint8_t val_len_len = 1;
    if (tlv->val_len > 0x7f) {
        uint8_t c = 0;
        size_t v = tlv->val_len;
        // loop to check number of bytes required
        while (v != 0) {
            v >>= 8;
            c++;
        }
        val_len_len += c;
    }
    return tag_len+val_len_len;
}

// locate a TLV object within a data buffer (recursively, depth-first)
BOOL TlvSearchTag(const uint8_t *buffer, size_t length, uint16_t tag,
                  BOOL recursive, Tlv_t* tlv)
{
    // extract first TLV object at current buffer position
    Tlv_t next_tlv;
    const uint8_t* next_buffer = ParseTlvHeader(buffer, length, &next_tlv);

    // DEBUG, copy data so we can print it! not useful for search algorithm
    //CopyTlvValueData(next_buffer, &next_tlv);
    //Tlv_Debug(&next_tlv);
    // DEBUG

    if (next_tlv.tag == tag) {
        //printf("FOUND!\n");
        TlvParse(buffer, length, tlv);
        return TRUE;
    } else if (!TlvPrimitive(&next_tlv)) {
        // constructed object so search recursively, if allowed
        return TlvSearchTag(next_buffer, next_tlv.val_len, tag, recursive, tlv);
    } else {
        // can't proceed further, attempt to look for next packet in depth!!
        uint8_t extra_bytes = GetTlvHeaderSize(&next_tlv);
        if (next_tlv.val_len+extra_bytes < length) {
            return TlvSearchTag(&buffer[next_tlv.val_len+extra_bytes],
                length-(next_tlv.val_len+extra_bytes),
                tag, recursive, tlv);
        } else {
            //printf("FAILED TO FIND\n");
            return FALSE;
        }
    }
}

// initialise container with supplied buffer and tag
BOOL TlvCreate(Tlv_t* tlv, uint16_t tag, uint8_t* buffer, size_t length)
{
    tlv->tag = tag;
    tlv->val_len = 0;
    tlv->buff = buffer;
    tlv->buff_len = length;
    return TRUE;
}

// write tag and length fields as bytes into buff
uint8_t* TlvEncodeHeader(uint8_t* buff, uint16_t tag, size_t len)
{
    uint8_t* p = buff;

    if (tag < 0xff) {
        // 1 byte tag
        *p++ = (tag & 0xff);
    } else {
        // 2 byte tag
        *p++ = (tag >> 8) & 0xff;
        *p++ = (tag & 0xff);
    }

    if (len <= 0x7f) {
        // 1 byte len
        *p++ = (len & 0xff);
    } else {
        uint8_t c = 0;
        size_t v = len;

        // multi-byte len
        *p++ = 0x80;
        do {
            *p++ = (v & 0xff);
            v >>= 8;
        } while (v != 0);
    }

    // pointer to next available byte in buffer
    return p;
}

// add a child TLV object into a TLV container...
// I assume this means to embed (copy) a TLV object within the data field?
BOOL TlvAdd(Tlv_t* tlv, const Tlv_t* childTlv)
{
    // check for free space
    uint8_t extra_bytes = GetTlvHeaderSize(childTlv);
    if (tlv->buff_len-tlv->val_len > extra_bytes+childTlv->val_len) {
        uint8_t *p;
        // insert encoded tag + length field!
        p = TlvEncodeHeader(&tlv->buff[tlv->val_len], childTlv->tag, childTlv->val_len);
        memcpy(p, childTlv->buff, childTlv->val_len);
        tlv->val_len += extra_bytes+childTlv->val_len;
    }
    return FALSE;
}

// add data into TLV container, as an embedded TLV data
BOOL TlvAddData(Tlv_t* tlv, uint16_t tag, uint8_t *value, size_t value_len)
{
    BOOL ret;

    // stick the new data into an object
    Tlv_t child_tlv;
    ret = TlvCreate(&child_tlv, tag, value, value_len);
    if (!ret) return FALSE;
    // copy data into it!
    child_tlv.val_len = value_len;
    CopyTlvValueData(value, &child_tlv);
    // then embed it using TlvAdd
    ret = TlvAdd(tlv, &child_tlv);
    if (!ret) return FALSE;

    return TRUE;
}

// just assume we have a nice big destination buffer
size_t EncodeTxnInfo(TxnInfo_t* txn, uint8_t *dest, size_t len)
{
    uint8_t* p;
    uint8_t buff[100];
    Tlv_t tlv;

    TlvCreate(&tlv, 0x2a, buff, len);
    TlvAddData(&tlv, 0x01, (uint8_t*)txn->TxnRef, strlen(txn->TxnRef)+1);
    TlvAddData(&tlv, 0x02, (uint8_t*)&txn->amount, sizeof(txn->amount));
    TlvAddData(&tlv, 0x03, (uint8_t*)&txn->txnType, sizeof(txn->txnType));
    TlvAddData(&tlv, 0x04, (uint8_t*)&txn->currencyCode, sizeof(txn->currencyCode));

    p = TlvEncodeHeader(dest, tlv.tag, tlv.val_len);
    memcpy(p, tlv.buff, tlv.val_len);
    return tlv.val_len+GetTlvHeaderSize(&tlv);
}

void DecodeTxnInfo(const uint8_t *buff, size_t len, TxnInfo_t* txn)
{
    Tlv_t tlv;

    TlvSearchTag(buff, len, 0x01, TRUE, &tlv);
    //Tlv_Debug(&tlv);
    strncpy(txn->TxnRef, (char *)tlv.buff, tlv.val_len);
    TlvSearchTag(buff, len, 0x02, TRUE, &tlv);
    memcpy(&txn->amount, tlv.buff, tlv.val_len);
    TlvSearchTag(buff, len, 0x03, TRUE, &tlv);
    memcpy(&txn->txnType, tlv.buff, tlv.val_len);
    TlvSearchTag(buff, len, 0x04, TRUE, &tlv);
    memcpy(&txn->currencyCode, tlv.buff, tlv.val_len);
}

// test data
uint8_t tlv1Data[] =
{
    0x70,0x43,0x5F,0x20,0x1A,0x56,0x49,0x53,
    0x41,0x20,0x41,0x43,0x51,0x55,0x49,0x52,
    0x45,0x52,0x20,0x54,0x45,0x53,0x54,0x20,
    0x43,0x41,0x52,0x44,0x20,0x32,0x39,0x57,
    0x11,0x47,0x61,0x73,0x90,0x01,0x01,0x00,
    0x10,0xD1,0x01,0x22,0x01,0x11,0x43,0x87,
    0x80,0x89,0x9F,0x1F,0x10,0x31,0x31,0x34,
    0x33,0x38,0x30,0x30,0x37,0x38,0x30,0x30,
    0x30,0x30,0x30,0x30,0x30,0x90,0x00
};

int main(void)
{
    uint8_t txn_buff[100];
    size_t txn_len;

    printf("Running some very basic functional tests\n\n");

    {
        Tlv_t my_tlv;
        printf("RECURSIVE TAG SEARCH FOR: 0x57\n");
        TlvSearchTag(tlv1Data, 71, 0x57, TRUE, &my_tlv);
        Tlv_Debug(&my_tlv);
    }

    {
        TxnInfo_t txn;
        strncpy(txn.TxnRef, "ABCDEFG", 16);
        txn.amount = 123;
        txn.txnType = 9;
        txn.currencyCode = 64;

        txn_len = EncodeTxnInfo(&txn, txn_buff, sizeof(txn_buff));
        printf("\nENCODED TRANSACTION:\n");
        PrintBufferHex(txn_buff, txn_len);
    }

    {
        TxnInfo_t txn;
        DecodeTxnInfo(txn_buff, txn_len, &txn);

        printf("\nDECODED TRANSACTION:\n");
        printf("TxnRef: %s\n", txn.TxnRef);
        printf("amount: %d\n", txn.amount);
        printf("txnType: %d\n", txn.txnType);
        printf("currencyCode: %d\n", txn.currencyCode);
    }

    //system("pause");  // make Windows happier
    return 0; 
}

